AutoSort
========

AutoSort is a chest inventory sorting plugin for Bukkit and Spigot. Please read the
[project page](http://dev.bukkit.org/bukkit-plugins/autosort/) on BukkitDev for more information.
