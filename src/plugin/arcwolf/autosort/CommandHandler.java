package plugin.arcwolf.autosort;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;
import plugin.arcwolf.autosort.Network.NetworkItem;
import plugin.arcwolf.autosort.Network.SortChest;
import plugin.arcwolf.autosort.Network.SortNetwork;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class CommandHandler {
    private enum Scope {
        PLAYERS,
        CONSOLE,
        BOTH
    }

    private final AutoSort plugin;
    private final BukkitScheduler scheduler;


    private abstract class AutoSortCommand implements CommandExecutor {
        private final Scope scope;

        public AutoSortCommand(Scope scope) {
            this.scope = scope;
        }

        public AutoSortCommand() {
            this(Scope.BOTH);
        }

        public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
            boolean isPlayer = sender instanceof Player;

            // Need to check if the scope of the command is restricted.
            if(scope == Scope.PLAYERS && ! isPlayer) {
                sender.sendMessage(ChatColor.RED + "This command can only be accessed by players in-game.");
                return true;
            } else if(scope == Scope.CONSOLE && isPlayer) {
                sender.sendMessage(ChatColor.RED + "This command can only be accessed on the console.");
                return true;
            }

            return runCommand(
                sender,
                cmd,
                commandLabel,
                args,
                isPlayer ? Scope.PLAYERS : Scope.CONSOLE
            );
        }

        public abstract boolean runCommand(CommandSender sender, Command cmd, String commandLabel,
                                           String[] args, Scope effectiveScope);
    }


    public CommandHandler(final AutoSort plugin) {
        this.plugin = plugin;
        scheduler = plugin.getServer().getScheduler();

commandLoop:
        for(String commandName : plugin.getDescription().getCommands().keySet()) {
            PluginCommand command = plugin.getCommand(commandName);
            CommandExecutor exec;

            switch(commandName) {
                case "autosort":
                case "autosortall":
                    exec = new AutoSortCommand(Scope.PLAYERS) {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd,
                                                  String commandLabel, String[] args, Scope effectiveScope) {
                            if (args.length < 1) {
                                return false;
                            }

                            boolean sortAll = cmd.getName().equals("autosortall");
                            Player player = (Player) sender;
                            SortNetwork net = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                player.getUniqueId(),
                                true
                            );

                            if (net == null) {
                                // Network not found; message already displayed by AutoSort::getNetworkFromNetspec().
                                return true;
                            }

                            if (!net.owner.equals(player.getUniqueId()) &&
                                !net.members.contains(player.getUniqueId()) &&
                                !plugin.hasPermission(player, "autosort.override")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sSorry, you are not a member of the %s%s%s network.",
                                        ChatColor.RED, ChatColor.YELLOW, net.netName, ChatColor.RED
                                    )
                                );
                                return true;
                            }

                            String owner = args[0].contains("/") ? args[0].split("/", 2)[0] : player.getName();
                            sortPlayerInventory(sortAll ? 0 : 9, sender, owner, net.netName, net);
                            return true;
                        }
                    };
                    break;

                case "aswithdraw":
                    exec = new AutoSortCommand(Scope.PLAYERS) {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if (args.length < 1) {
                                return false;
                            }

                            Player currentPlayer = (Player) sender;
                            SortNetwork net = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                currentPlayer.getUniqueId(),
                                true
                            );

                            if (net == null) {
                                // Network not found; message already displayed by AutoSort::getNetworkFromNetspec().
                                return true;
                            }

                            if (net.netName.equalsIgnoreCase("$Public") ||
                                net.members.contains(currentPlayer.getUniqueId()) ||
                                net.owner.equals(currentPlayer.getUniqueId()) ||
                                plugin.hasPermission(currentPlayer, "autosort.override")) {
                                doCommandWithdraw(currentPlayer, net, net.owner, net.netName);
                            } else {
                                sender.sendMessage(
                                    ChatColor.RED + "Sorry you are not a member of the " + ChatColor.YELLOW + net.netName
                                        + ChatColor.WHITE + " network."
                                );
                            }

                            return true;
                        }
                    };
                    break;

                case "asreload":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel,
                                                  String[] args, Scope effectiveScope) {
                            reload(sender);
                            return true;
                        }
                    };
                    break;

                case "asconvertids":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender,
                                                  Command cmd,
                                                  String commandLabel,
                                                  String[] args,
                                                  Scope effectiveScope) {
                            plugin.configConvertIdsToNames();
                            sender.sendMessage(
                                ChatColor.GREEN + "Material IDs in AutoSort config successfully converted to names."
                            );
                            return true;
                        }
                    };
                    break;

                case "addtonet":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if(args.length < 2) {
                                return false;
                            }

                            UUID currentPlayer = effectiveScope == Scope.PLAYERS
                                ? ((Player)sender).getUniqueId()
                                : null;
                            SortNetwork net = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                currentPlayer,
                                true
                            );

                            if(net == null) {
                                // Network not found; message already displayed by AutoSort::getNetworkFromNetspec().
                                return true;
                            }

                            if (net.netName.equalsIgnoreCase("$Public")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sPublic networks allow everyone already.",
                                        ChatColor.YELLOW
                                    )
                                );
                                return true;
                            }

                            if(currentPlayer != null && ! net.owner.equals(currentPlayer) &&
                                ! plugin.hasPermission((Player)sender, "autosort.override")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sYou are not allowed to add players to the networks of other players.",
                                        ChatColor.RED
                                    )
                                );
                                return true;
                            }

                            int count = 0;
                            for (int i = 1; i < args.length; i++) {
                                UUID memberId = getPlayerUUID(args[i], sender);
                                if (memberId == null)
                                    continue;

                                if (net.members.contains(memberId)) {
                                    sender.sendMessage(ChatColor.YELLOW + args[i] + " already added to the network.");
                                } else {
                                    net.members.add(memberId);
                                    count++;
                                }
                            }

                            sender.sendMessage(
                                count + " " + ChatColor.BLUE + "Player(s) successfully added to the network."
                            );
                            return true;
                        }
                    };
                    break;

                case "remfromnet":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if (args.length < 2) {
                                return false;
                            }

                            UUID currentPlayer = effectiveScope == Scope.PLAYERS
                                ? ((Player) sender).getUniqueId()
                                : null;
                            SortNetwork net = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                currentPlayer,
                                true
                            );

                            if (net == null) {
                                // Network not found; message already displayed by AutoSort::getNetworkFromNetspec().
                                return true;
                            }

                            if (net.netName.equalsIgnoreCase("$Public")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sPublic networks allow everyone.",
                                        ChatColor.YELLOW
                                    )
                                );
                                return true;
                            }

                            if (currentPlayer != null && !net.owner.equals(currentPlayer) &&
                                !plugin.hasPermission((Player) sender, "autosort.override")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sYou are not allowed to remove players from the networks of other players.",
                                        ChatColor.RED
                                    )
                                );
                                return true;
                            }

                            int count = 0;
                            for (int i = 1; i < args.length; i++) {
                                UUID memberId = getPlayerUUID(args[0], sender);
                                if (memberId == null)
                                    continue;
                                if (!net.members.contains(memberId)) {
                                    sender.sendMessage(ChatColor.YELLOW + args[i] + " is not a member of the network.");
                                } else {
                                    net.members.remove(memberId);
                                    count++;
                                }
                            }

                            sender.sendMessage(
                                count + " " + ChatColor.BLUE + "Player(s) successfully removed from the network."
                            );
                            return true;
                        }
                    };
                    break;

                case "addasgroup":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if(args.length < 2) {
                                return false;
                            }

                            String groupName = args[0].toUpperCase();
                            List<ItemStack> matList = new ArrayList<ItemStack>();
                            List<String> ids = new ArrayList<String>();
                            for (int i = 1; i < args.length; i++) {
                                String mat = args[i];
                                Pattern matPat;

                                try {
                                    matPat = Util.patternFromConfigString(mat);
                                } catch (PatternSyntaxException e) {
                                    sender.sendMessage(ChatColor.RED + "Invalid regular expression: " + mat);
                                    continue;
                                }

                                if (matPat != null) {
                                    matList.addAll(plugin.itemDb.getItemStacksForRegex(matPat));
                                    ids.add(mat);
                                } else if (Util.parseMaterialID(mat) != null) {
                                    matList.add(Util.parseMaterialID(mat));
                                    ids.add(mat);
                                } else {
                                    sender.sendMessage(ChatColor.RED + "Invalid Material: " + mat);
                                }
                            }

                            ConfigurationSection groupsSec = plugin.getConfig().getConfigurationSection("customGroups");
                            groupsSec.set(groupName, ids);
                            plugin.saveConfig();

                            AutoSort.customMatGroups.put(groupName, matList);

                            sender.sendMessage(ChatColor.GREEN + "AutoSort group added.");
                            return true;
                        }
                    };
                    break;

                case "modasgroup":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if (args.length < 2) {
                                return false;
                            }

                            String groupName = args[0].toUpperCase();
                            if (AutoSort.customMatGroups.containsKey(groupName)) {
                                ConfigurationSection customGroups = plugin.getConfig().getConfigurationSection(
                                    "customGroups"
                                );
                                Set<String> ids = new LinkedHashSet<>(customGroups.getStringList(groupName));
                                Set<ItemStack> matList = new HashSet<>(AutoSort.customMatGroups.get(groupName));

                                for (int i = 1; i < args.length; i++) {
                                    String mat = args[i];
                                    Pattern matPat;

                                    try {
                                        matPat = Util.patternFromConfigString(mat);
                                    } catch (PatternSyntaxException e) {
                                        sender.sendMessage(ChatColor.RED + "Invalid regular expression: " + mat);
                                        continue;
                                    }

                                    if (matPat != null) {
                                        matList.addAll(plugin.itemDb.getItemStacksForRegex(matPat));
                                        ids.add(mat);
                                    } else if (Util.parseMaterialID(mat) != null) {
                                        matList.add(Util.parseMaterialID(mat));
                                        ids.add(mat);
                                    } else if (args[i].startsWith("-")) {
                                        String toRemove = args[i].substring(1);
                                        ids.remove(toRemove);

                                        // TODO: Handle regular expressions as well (remove all matching items).
                                        ItemStack parsedItem = Util.parseMaterialID(toRemove);
                                        if(parsedItem == null) {
                                            continue;
                                        }

                                        Iterator<ItemStack> itms = matList.iterator();
                                        while (itms.hasNext()) {
                                            ItemStack item = itms.next();

                                            if (item.getTypeId() == parsedItem.getTypeId() &&
                                                    item.getDurability() == parsedItem.getDurability())
                                                itms.remove();
                                        }
                                    } else {
                                        sender.sendMessage(ChatColor.RED + "Invalid Material: " + mat);
                                    }
                                }

                                customGroups.set(groupName, new ArrayList<>(ids));
                                plugin.saveConfig();

                                AutoSort.customMatGroups.put(groupName, new ArrayList<>(matList));

                                sender.sendMessage(ChatColor.GREEN + "AutoSort group modified.");
                            } else {
                                sender.sendMessage(ChatColor.RED + "That group does not exist!");
                            }

                            return true;
                        }
                    };
                    break;

                case "delasgroup":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if(args.length < 1) {
                                return false;
                            }

                            String groupName = args[0].toUpperCase();
                            if (AutoSort.customMatGroups.containsKey(groupName)) {
                                ConfigurationSection groupsSec = plugin.getConfig().getConfigurationSection(
                                    "customGroups"
                                );
                                groupsSec.set(groupName, null);
                                plugin.saveConfig();

                                AutoSort.customMatGroups.remove(groupName);

                                sender.sendMessage(ChatColor.GREEN + "AutoSort group deleted.");
                            } else {
                                sender.sendMessage(ChatColor.RED + "That group does not exist!");
                            }

                            return true;
                        }
                    };
                    break;

                case "ascleanup":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            sender.sendMessage(ChatColor.BLUE + "Cleaning up all AutoSort networks...");
                            AutoSort.LOGGER.info("AutoSort: Command Cleanup Process Started.");

                            if (!plugin.cleanupNetwork())
                                AutoSort.LOGGER.info("AutoSort: All networks are clean.");

                            sender.sendMessage("Check server log for information on cleanup procedure.");
                            AutoSort.LOGGER.info("AutoSort: Finished Command Cleanup Process.");
                            sender.sendMessage(ChatColor.BLUE + "Done.");

                            return true;
                        }
                    };
                    break;

                case "listasgroups":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            ConfigurationSection customGroups = plugin.getConfig().getConfigurationSection(
                                "customGroups"
                            );
                            List<String> groupNames = new ArrayList<>(customGroups.getKeys(false));
                            Collections.sort(groupNames);

                            sender.sendMessage(ChatColor.GOLD + "Custom AutoSort material groups:");
                            for (String groupName : groupNames) {
                                String msg = String.format(
                                    "%s%s%s: ",
                                    ChatColor.WHITE, groupName, ChatColor.GOLD
                                );

                                List<String> mats = new LinkedList<>();
                                for (String groupItem : customGroups.getStringList(groupName)) {
                                    Pattern matPat;
                                    try {
                                        matPat = Util.patternFromConfigString(groupItem);
                                    } catch (PatternSyntaxException e) {
                                        continue;
                                    }

                                    if (matPat == null) {
                                        // Not a regex.
                                        mats.add(groupItem);
                                    } else {
                                        // It's a regex!
                                        String matchMsg = ChatColor.LIGHT_PURPLE + groupItem + ChatColor.WHITE + " ("
                                            + ChatColor.GOLD;
                                        List<String> matchedNames = plugin.itemDb.getNamesForRegex(matPat);

                                        if (matchedNames.size() > 0) {
                                            Collections.sort(matchedNames);
                                            matchMsg += Util.joinCollectionWith(
                                                matchedNames,
                                                ChatColor.WHITE + ", " + ChatColor.GOLD
                                            );
                                        } else {
                                            matchMsg += "<no matches>";
                                        }

                                        matchMsg += ChatColor.WHITE + ")";
                                        mats.add(matchMsg);
                                    }
                                }

                                Collections.sort(mats);
                                msg += Util.joinCollectionWith(
                                    mats,
                                    ChatColor.WHITE + ", " + ChatColor.GOLD
                                );
                                sender.sendMessage(msg);
                            }

                            return true;
                        }
                    };
                    break;

                case "listasmembers":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if(args.length < 1) {
                                return false;
                            }

                            SortNetwork network = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                effectiveScope == Scope.PLAYERS ? ((Player)sender).getUniqueId() : null,
                                true
                            );
                            if(network == null) {
                                return true;
                            }

                            listMembers(sender, network);
                            return true;
                        }
                    };
                    break;

                case "asremnet":
                    exec = new AutoSortCommand() {
                        @Override
                        public boolean runCommand(CommandSender sender, Command cmd, String commandLabel, String[] args,
                                                  Scope effectiveScope) {
                            if (args.length < 1) {
                                return false;
                            }

                            UUID currentPlayer = effectiveScope == Scope.PLAYERS
                                ? ((Player) sender).getUniqueId()
                                : null;
                            SortNetwork net = plugin.getNetworkFromNetspec(
                                args[0],
                                sender,
                                currentPlayer,
                                true
                            );

                            if (net == null) {
                                // Network not found; message already displayed by AutoSort::getNetworkFromNetspec().
                                return true;
                            }

                            if (currentPlayer != null && !net.owner.equals(currentPlayer) &&
                                !plugin.hasPermission((Player) sender, "autosort.override")) {
                                sender.sendMessage(
                                    String.format(
                                        "%sYou are not allowed to remove players from the networks of other players.",
                                        ChatColor.RED
                                    )
                                );
                                return true;
                            }

                            if (!deleteNetwork(sender, net, sender.getName()))
                                return true;

                            String ownerName = args[0].contains("/") ? args[0].split("/", 2)[0] : sender.getName();
                            sender.sendMessage(
                                String.format(
                                    "%sThe network %s%s%s owned by %s%s%s has been deleted.",
                                    ChatColor.YELLOW, ChatColor.WHITE, net.netName, ChatColor.YELLOW,
                                    ChatColor.WHITE, ownerName, ChatColor.YELLOW
                                )
                            );

                            plugin.saveVersion6Network();
                            return true;
                        }
                    };
                    break;

                default:
                    AutoSort.LOGGER.warning(
                        String.format(
                            "AutoSort: Command /%s registered in plugin.yml, but not handled! Command will do nothing!",
                            commandName
                        )
                    );
                    continue commandLoop;
            }

            command.setExecutor(exec);
        }
    }

    private UUID getPlayerUUID(String name, CommandSender sender) {
        UUID uuid = FindUUID.getUUIDFromPlayerName(name);
        if (uuid == null) {
            sender.sendMessage(ChatColor.RED + "The player name " + ChatColor.YELLOW + "'" + name + "'" + ChatColor.RED + " could not be found.");
        }
        return uuid;
    }

    private void reload(final CommandSender sender) {
        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {

            @Override
            public void run() {
                try {
                    sender.sendMessage(ChatColor.AQUA + "AutoSort reloading...");
                    CustomPlayer.playerSettings.clear();
                    plugin.items.clear();
                    plugin.stillItems.clear();
                    plugin.allNetworkBlocks.clear();
                    plugin.networks.clear();
                    plugin.sortBlocks.clear();
                    plugin.depositBlocks.clear();
                    plugin.withdrawBlocks.clear();
                    AutoSort.customMatGroups.clear();
                    AutoSort.proximities.clear();
                    sender.sendMessage(ChatColor.YELLOW + "AutoSort variables cleared.");

                    plugin.loadConfig();
                    sender.sendMessage(ChatColor.YELLOW + "AutoSort config reloaded.");
                    plugin.loadCustomGroups();
                    sender.sendMessage(ChatColor.YELLOW + "AutoSort custom groups reloaded.");
                    plugin.loadInventoryBlocks();
                    sender.sendMessage(ChatColor.YELLOW + "AutoSort inventory block list reloaded.");
                    plugin.loadDatabase();
                    sender.sendMessage(ChatColor.YELLOW + "AutoSort database reloaded.");
                    sender.sendMessage(ChatColor.GREEN + "AutoSort reload finished successfully.");
                } catch (Exception e) {
                    e.printStackTrace();
                    sender.sendMessage(ChatColor.RED + "AutoSort reload failed.");
                }
            }
        }, 0);
    }

    private boolean deleteNetwork(CommandSender player, SortNetwork network, String whoDeleted) {
        if (checkIfInUse(player, network)) {
            return false;
        }

        List<Block> netItemsToDel = new ArrayList<Block>();
        for(Entry<Block, NetworkItem> wchest : network.withdrawChests.entrySet()) {
            if (wchest.getValue().network.equals(network)) {
                plugin.allNetworkBlocks.remove(wchest.getValue().chest);
                plugin.allNetworkBlocks.remove(plugin.util.doubleChest(wchest.getValue().chest));
                plugin.allNetworkBlocks.remove(wchest.getValue().sign);
                updateSign(wchest.getValue().sign, network.netName, whoDeleted);
                netItemsToDel.add(wchest.getKey());
            }
        }
        for(Entry<Block, NetworkItem> dchest : network.depositChests.entrySet()) {
            if (dchest.getValue().network.equals(network)) {
                plugin.allNetworkBlocks.remove(dchest.getValue().chest);
                plugin.allNetworkBlocks.remove(plugin.util.doubleChest(dchest.getValue().chest));
                plugin.allNetworkBlocks.remove(dchest.getValue().sign);
                updateSign(dchest.getValue().sign, network.netName, whoDeleted);
                netItemsToDel.add(dchest.getKey());
            }
        }
        for(Entry<Block, NetworkItem> dsign : network.dropSigns.entrySet()) {
            if (dsign.getValue().network.equals(network)) {
                plugin.allNetworkBlocks.remove(dsign.getValue().sign);
                updateSign(dsign.getValue().sign, network.netName, whoDeleted);
                netItemsToDel.add(dsign.getKey());
            }
        }
        for(SortChest chest : network.sortChests) {
            plugin.allNetworkBlocks.remove(chest.block);
            plugin.allNetworkBlocks.remove(plugin.util.doubleChest(chest.block));
            plugin.allNetworkBlocks.remove(chest.sign);
            updateSign(chest.sign, network.netName, whoDeleted);
        }
        for(Block netBlock : netItemsToDel) {
            network.depositChests.remove(netBlock);
            network.depositChests.remove(plugin.util.doubleChest(netBlock));
            network.withdrawChests.remove(netBlock);
            network.withdrawChests.remove(plugin.util.doubleChest(netBlock));
            network.dropSigns.remove(netBlock);
        }
        plugin.networks.get(network.owner).remove(network);
        return true;
    }

    private void updateSign(final Block sign, final String netName, final String whoDeleted) {
        scheduler.runTask(plugin, new Runnable() {

            @Override
            public void run() {
                if (sign.getType().equals(Material.WALL_SIGN) || sign.getType().equals(Material.SIGN_POST)) {
                    BlockState sgn = sign.getState();
                    Sign s = (Sign) sign.getState();
                    s.setLine(0, ChatColor.YELLOW + "[ " + netName + " ]");
                    s.setLine(1, ChatColor.YELLOW + "deleted by");
                    s.setLine(2, ChatColor.YELLOW + whoDeleted);
                    s.setLine(3, "");
                    sgn.update(true);
                    s.update(true);
                }
            }
        });
    }

    private void sortPlayerInventory(final int startIndex, final CommandSender sender, final String owner, final String netName, final SortNetwork net) {
        scheduler.runTask(plugin, new Runnable() {

            @Override
            public void run() {
                Player player = (Player) sender;
                Inventory inv = player.getInventory();
                ItemStack[] contents = inv.getContents();
                ItemStack is;
                for(int i = startIndex; i < contents.length; i++) {
                    is = contents[i];
                    if (is != null) {
                        if (net.sortItem(is)) {
                            contents[i] = null;
                        }
                    }
                }
                inv.setContents(contents);
                sender.sendMessage(ChatColor.GREEN + "Inventory sorted into " + ChatColor.YELLOW + netName + ChatColor.WHITE + " owned by " + ChatColor.YELLOW + owner);
            }
        });
    }

    private boolean checkIfInUse(CommandSender player, SortNetwork network) {
        if (plugin.asListener.chestLock.containsValue(network)) {
            String user = "";
            for(Entry<String, SortNetwork> sortNet : plugin.asListener.chestLock.entrySet()) {
                if (sortNet.getValue().equals(network)) {
                    user = sortNet.getKey();
                    break;
                }
            }
            //Transaction Fail someone else is using the withdraw function
            player.sendMessage("The network " + ChatColor.YELLOW + network.netName + ChatColor.WHITE + " is being withdrawn from by " + ChatColor.YELLOW + user);
            player.sendMessage(ChatColor.GOLD + "Please wait...");
            return true;
        }
        return false;
    }

    private boolean doCommandWithdraw(final Player player, SortNetwork network, UUID owner, final String netName) {
        final CustomPlayer settings = CustomPlayer.getSettings(player);
        if (checkIfInUse(player, network)) return true;
        plugin.asListener.chestLock.put(player.getName(), network);
        settings.netName = netName;
        settings.owner = owner;
        settings.playerName = player.getName();
        settings.sortNetwork = network;
        settings.withdrawInventory = Bukkit.createInventory(null, 54, netName + " network inventory");
        scheduler.runTask(plugin, new Runnable() {

            @Override
            public void run() {
                if (plugin.util.updateInventoryList(player, settings)) {
                    Collections.sort(settings.inventory, new StringComparator());
                    plugin.util.updateChestInventory(player, settings);
                    player.openInventory(settings.withdrawInventory);
                }
                else {
                    player.sendMessage("The network - " + ChatColor.YELLOW + netName + ChatColor.WHITE + " - is empty.");
                    plugin.asListener.chestLock.remove(player.getName());
                    settings.clearPlayer();
                }
            }
        });
        return true;
    }

    private boolean listMembers(CommandSender sender, SortNetwork network) {
        if (network.members.size() == 0) {
            sender.sendMessage(ChatColor.RED + "There are no members of network " + ChatColor.RESET + network.netName + ChatColor.RED + " owned by " + ChatColor.RESET + network.owner);
            return true;
        }
        StringBuilder sb = new StringBuilder();
        String name, netName = network.netName;
        sender.sendMessage("Network: " + ChatColor.GOLD + netName);
        sb.append("Members: ");
        sb.append(ChatColor.AQUA);
        for(int i = 0; i < network.members.size(); i++) {
            OfflinePlayer op = Bukkit.getOfflinePlayer(network.members.get(i));
            if (op == null) continue;
            name = op.getName() == null ? "Unknown: " + network.members.get(i).toString() : op.getName();
            if ((sb.length() + name.length()) > 80) {
                sender.sendMessage(sb.toString());
                sb = new StringBuilder();
                sb.append(ChatColor.AQUA);
            }
            if (i < network.members.size() - 1) {
                sb.append(name);
                sb.append(ChatColor.RESET);
                sb.append(", ");
                sb.append(ChatColor.AQUA);
            }
            else
                sb.append(name);
        }
        sender.sendMessage(sb.toString());
        return true;
    }
}