package plugin.arcwolf.autosort;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItemDatabase {
    private String dbPath;
    private LineNumberReader dbReader;

    private ConcurrentMap<ItemTuple, Set<String>> itemTupleToNames = new ConcurrentHashMap<>();
    private ConcurrentMap<String, ItemStack> strToItemStack = new ConcurrentHashMap<>();
    private ConcurrentMap<HashablePattern, Set<ItemStack>> regexCache = new ConcurrentHashMap<>();


    private static class ItemTuple {
        public final int itemId;
        public final short itemDurability;

        public ItemTuple(int itemId, short itemDurability) {
            this.itemId = itemId;
            this.itemDurability = itemDurability;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            ItemTuple itemTuple = (ItemTuple) o;
            return itemId == itemTuple.itemId && itemDurability == itemTuple.itemDurability;
        }

        @Override
        public int hashCode() {
            int result = itemId;
            result = 31 * result + (int) itemDurability;
            return result;
        }
    }

    private static class HashablePattern {
        private String pattern;
        private int flags;

        public HashablePattern(Pattern pat) {
            pattern = pat.pattern();
            flags = pat.flags();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            HashablePattern that = (HashablePattern) o;
            return flags == that.flags && pattern.equals(that.pattern);
        }

        @Override
        public int hashCode() {
            int result = pattern.hashCode();
            result = 31 * result + flags;
            return result;
        }
    }


    public ItemDatabase(InputStream dbStream, String dbPath) {
        this.dbPath = dbPath;
        this.dbReader = new LineNumberReader(
            new BufferedReader(
                new InputStreamReader(
                    dbStream,
                    StandardCharsets.UTF_8
                )
            )
        );
    }

    public ItemDatabase(InputStream dbStream) {
        this(dbStream, "<unknown>");
    }

    public ItemDatabase(String dbPath) throws IOException {
        this(
            new FileInputStream(dbPath),
            dbPath
        );
    }

    public void parse() throws IOException {
        String line;
        while ((line = dbReader.readLine()) != null) {
            line = line.trim();

            if (line.isEmpty() || line.startsWith("#")) {
                // Ignore empty lines and comments.
                continue;
            }

            String[] parts = line.split("\\s*,\\s*");
            if (parts.length < 3) {
                AutoSort.LOGGER.warning(
                    String.format(
                        "AutoSort: Invalid line in item database %s:%d: Need three values.",
                        dbPath, dbReader.getLineNumber()
                    )
                );
                continue;
            }

            String itemName = parts[0].toUpperCase();
            Integer itemId;
            Short itemDurability;

            try {
                itemId = Integer.valueOf(parts[1]);
                itemDurability = Short.valueOf(parts[2]);
            } catch (NumberFormatException e) {
                AutoSort.LOGGER.warning(
                    String.format(
                        "AutoSort: Invalid line in item database %s:%d: Item ID and item metadata need to be numeric.",
                        dbPath, dbReader.getLineNumber()
                    )
                );
                continue;
            }

            // Create the tuple-to-string-list mapping.
            ItemTuple tuple = new ItemTuple(itemId, itemDurability);
            if (!itemTupleToNames.containsKey(tuple)) {
                itemTupleToNames.put(tuple, Collections.synchronizedSet(new LinkedHashSet<String>()));
            }

            itemTupleToNames.get(tuple).add(itemName);

            // Create the item-name-to-stack mapping.
            ItemStack stack = new ItemStack(itemId, 1, itemDurability);
            strToItemStack.put(itemName, stack);
        }
    }

    public Set<String> getNamesForItemStack(ItemStack stack) {
        return itemTupleToNames.get(
            new ItemTuple(stack.getTypeId(), stack.getDurability())
        );
    }

    public String getFirstNameForItemStack(ItemStack stack) {
        Set<String> allNames = getNamesForItemStack(stack);

        if (allNames == null) {
            return null;
        }

        return allNames.iterator().next();
    }

    public ItemStack getItemStackByName(String name) {
        return strToItemStack.get(name.toUpperCase());
    }

    public Set<ItemStack> getItemStacksForRegex(Pattern regex) {
        HashablePattern hashPat = new HashablePattern(regex);

        if(regexCache.containsKey(hashPat)) {
            return regexCache.get(hashPat);
        }

        // Else we have to generate the item cache... Fun!
        Set<ItemStack> stacks = Collections.synchronizedSet(new HashSet<ItemStack>());
        for(Map.Entry<String, ItemStack> entry : strToItemStack.entrySet()) {
            Matcher itemMatcher = regex.matcher(entry.getKey());
            if(itemMatcher.find()) {
                stacks.add(entry.getValue());
            }
        }

        // To be sure, also query Minecraft's built-in material list.
        for(Material mat : Material.values()) {
            Matcher itemMatcher = regex.matcher(mat.name());
            if (itemMatcher.find()) {
                stacks.add(new ItemStack(mat, 1));
            }
        }

        regexCache.put(hashPat, stacks);
        return stacks;
    }

    public List<String> getNamesForRegex(Pattern regex) {
        List<String> names = new LinkedList<>();

        for(ItemStack stack : getItemStacksForRegex(regex)) {
            String name = getFirstNameForItemStack(stack);

            if(name != null) {
                names.add(name);
            } else if(stack.getDurability() == 0) {
                // It's probably a Minecraft material not contained in the DB.
                names.add(stack.getType().name());
            } else {
                // A Minecraft material which is not in the DB, but with metadata... Cannot simply
                // query its name since that name does not reflect the metadata.
                names.add(stack.getType().name() + ":" + stack.getDurability());
            }
        }

        return names;
    }
}