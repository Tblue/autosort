Third-party licenses
====================

This repository contains certain third-party packages which are released under their own licenses;
they are listed in this file.

BKCommonLib
-----------

The [BKCommonLib utility library and Minecraft API](http://dev.bukkit.org/bukkit-plugins/bkcommonlib/) is released under the following license:

    BKCommonLib is an utility library made to supply Minecraft API functionality
    Copyright (C) 2011  bergerkiller (Irmo van den Berge)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

bPermissions
------------

The [bPermissions Superperms Manager for Bukkit](http://dev.bukkit.org/bukkit-plugins/bpermissions/) is released under the following license:

    Attribute Only (Public) License
        Version 0.a3, July 11, 2011

    Copyright (C) 2011 Nijikokun <nijikokun@gmail.com> (@nijikokun)

    Anyone is allowed to copy and distribute verbatim or modified
    copies of this license document and altering is allowed as long
    as you attribute the author(s) of this license document / files.

    ATTRIBUTE ONLY PUBLIC LICENSE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

      1. Attribute anyone attached to the license document.
         * Do not remove pre-existing attributes.

         Plausible attribution methods:
            1. Through comment blocks.
            2. Referencing on a site, wiki, or about page.

      2. Do whatever you want as long as you don't invalidate 1.

    @license AOL v.a3 <http://aol.nexua.org>

Essentials Group Manager
------------------------

[Essentials Group Manager](http://wiki.ess3.net/wiki/Group_Manager), part of the
[Essentials Minecraft server command mod](https://github.com/essentials/Essentials),
is released under the following license:

    Essentials - a bukkit plugin
    Copyright (C) 2011  Essentials Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

Lava Furnace
------------

The [Lava Furnace](http://dev.bukkit.org/bukkit-plugins/lavafurnace/) plugin for Bukkit
by [arcvvolf](http://dev.bukkit.org/profiles/arcvvolf/) is released under the following license:

    All Rights Reserved unless otherwise explicitly stated.

_NB: The JAR file for this plugin was committed to this repository in revision
009bcd7ef3c01f9ec131b9e724740ce0b3e64b19 by the plugin's author, arcvvolf
(a. k. a. [arcwolf](https://github.com/arcwolf))._

LWC
---

The [LWC block protection plugin](http://dev.bukkit.org/bukkit-plugins/lwc/) is released under the following
license:

    Copyright 2011 Tyler Blair. All rights reserved.

    Redistribution and use in source and binary forms, with or without modification, are
    permitted provided that the following conditions are met:

       1. Redistributions of source code must retain the above copyright notice, this list of
          conditions and the following disclaimer.

       2. Redistributions in binary form must reproduce the above copyright notice, this list
          of conditions and the following disclaimer in the documentation and/or other materials
          provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are those of the
    authors and contributors and should not be interpreted as representing official policies,
    either expressed or implied, of anybody else.

PermissionsEx
-------------

The [PermissionsEx](http://dev.bukkit.org/bukkit-plugins/permissionsex/) plugin is released
under the following license:

    PermissionsEx
    Copyright (C) zml and PermissionsEx contributors

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

Permissions
-----------

The (now discontinued)
[Permissions](https://bukkit.org/threads/admn-dev-permissions-3-1-6-the-plugin-of-tomorrow-935.18430/)
plugin for Bukkit is released under the following license:

    Permissions 3.x
    Copyright (C) 2011 Matt 'The Yeti' Burnett <admin@theyeticave.net>
    Original Credit & Copyright (C) 2010 Nijikokun <nijikokun@gmail.com>

    This program is free software: you can redistribute it and/or modify it under the
    terms of the GNU Permissions Public License as published by the Free Software
    Foundation, either version 2 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. See the GNU Permissions Public License for more details.

    You should have received a copy of the GNU Permissions Public License along with this
    program. If not, see <http://www.gnu.org/licenses/>.

Vault
-----

The [Vault Abstraction Library for Bukkit](https://github.com/MilkBowl/Vault/) has been
released under the following license:

    Copyright (C) 2011 Morgan Humes morgan@lanaddict.com

    Vault is free software: you can redistribute it and/or modify it under the terms of the GNU
    Lesser General Public License as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Vault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
    even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License along with Vault.
    If not, see http://www.gnu.org/licenses/.
